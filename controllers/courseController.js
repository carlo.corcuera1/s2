const Course = require("../models/Course");

module.exports.addCourse = (data) => {

	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {

			// Course creation successful
			if (error) {

				return false;

			// Course creation failed
			} else {

				return {
					message: "New Course successfully created!"
				};

			};

		});

	// User is not an admin
	} 

	// else {
	// 	return false
	// }

	let message = Promise.resolve({
		message: "User must be an Admin to access this!"
	})

	 return message.then((value) => {
	 	return value;
	 })

};

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result
	});
};

module.exports.getAllActive = () => {

	return Course.find({isActive : true}).then(result => {
		return result;
	});

};

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});

};

module.exports.updateCourse = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated
	let updatedCourse = {
		name : reqBody.name,
		description	: reqBody.description,
		price : reqBody.price
	};

	// Syntax
		// findByIdAndUpdate(document ID, updatesToBeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

		// Course not updated
		if (error) {

			return false;

		// Course updated successfully
		} else {

			let message = `Successfully updating course Id - "${reqParams.courseId}"`
			
			return message;
		};

	});

};

module.exports.archiveCourse = (reqParams) => {

	let updatedActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedActiveField).then((course, error) => {

		if(error){
			return false;
		} else {
			return true;
		};
	});
};