const User = require("../models/User"); 
const bcrypt = require("bcrypt");
const auth = require("../auth");

const Course = require("../models/Course");

module.exports.checkEmailExists = (reqBody) => {

	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {

		// The "find" method returns a record if a match is found
		if (result.length > 0) {

			return true;

		// No duplicate email found
		// The user is not yet registered in the database
		} else {

			return false;

		};
	});

};


module.exports.registerUser = (reqBody) => {

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {

		// User registration failed
		if (error) {

			return false;

		// User registration successful

		} else {

			return true;

		};

	});

}

module.exports.loginUser = (reqBody) => {
	// The "findOne" method returns the first record in the collection that matches the search criteria
	// We use the "findOne" method instead of the "find" method which returns all records that match the search criteria
	return User.findOne({email : reqBody.email}).then(result => {

		// User does not exist
		if(result == null){

			return false;

		// User exists
		} else {

			// Creates the variable "isPasswordCorrect" to return the result of comparing the login form password and the database password
			// The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
			// A good coding practice for boolean variable/constants is to use the word "is" or "are" at the beginning in the form of is+Noun
				//example. isSingle, isDone, isAdmin, areDone, etc..
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

			// If the passwords match/result of the above code is true
			if (isPasswordCorrect) {

				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
				return { access : auth.createAccessToken(result) }

			// Passwords do not match
			} else {

				return false;

			};

		};

	});

};

// Activity

/*module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};*/

// Retrieve User Details
module.exports.getProfile = (userData) => {
    return User.findOne({id : userData._id}).then(result => {
        if (result == null){
            return false
        } else {
            result.password = ""
            return result
        }
    });
};

		
	// Async await will be used in enrolling the user because we will need to update 2 separate documents when enrolling a user
module.exports.enroll = async (data) => {
	// "async" is placed in line 119 to decalare that this is an async expression.
	// Given that this is an async expression, "await" keyword should be permitted within the body. 

		// An async expression, is an expression that returns a promise. 

	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// await was written in line 125, so that, this tells our code to wait for the promise to resolve, in this case for our course enrollees to be "push" in our database. 
		// Of course, given that this will return a "no error" & a successful one, this will now be assigned to variable isCourseUpdated 

		course.enrollees.push({userId : data.userId });

		return course.save().then((course, error) => {
			if(error){
				return false
			} else {
				return true
			};
		});
	});

	let isUserUpdated = await User.findById(data.userId).then(user => {
		// await is also written in line 140, so that, it wait for the promise to be resolve, in this case for our user enrollments to be "push" in our database
		// Again, given that this will return true, this will now be assigned to variable isCourseUpdated

		user.enrollments.push({courseId : data.courseId});

		return user.save().then((user, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	// To check if we are successful in fulfilling both promise, we will utilize the if statement and the double ampersand as repsentation of logical "AND". 

	if(isUserUpdated && isCourseUpdated){

		return true;

	} else {

		return false;

	}

}


