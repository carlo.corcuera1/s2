const express = require("express");
const router = express.Router();

const courseController = require("../controllers/courseController");

const auth = require("../auth"); // Added in s39 activity

// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));

});

// Route for retrieving all the courses // added in s40
router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})

// Route for retrieving all Active courses // added in s40
router.get("/active", (req, res) => {

	courseController.getAllActive().then(resultFromController => res.send(resultFromController));

});

// Route for retrieving a specific course
router.get("/:courseId", (req, res) => {

	console.log(req.params.courseId);

	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the url
		// Example: URL - http://localhost:4000/courses/613e926a82198824c8c4ce0e
		// The course Id is "613e926a82198824c8c4ce0e" which is passed via the url that corresponds to the "courseId" in the route
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));

});

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));

});

// Activity Solution

// Route for archiving a course
router.put("/:courseId/archive", auth.verify, (req, res) => {

	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
})

module.exports = router;