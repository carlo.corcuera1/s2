const express = require("express");
const router = express.Router();

const auth = require("../auth"); // Added in s39 discussion

const userController = require("../controllers/userController")

// Route for checking if the user's email already exists in the database
// Invokes the checkEmailExists function from the controller file to communicate with our database
// Passes the "body" property of our "request" object to the corresponding controller function
router.post("/checkEmail", (req, res) => {
	
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));

});

router.post("/register", (req, res) => {

	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));

});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
// The "auth.verity" acts as a middleware to ensure that the user is logged in before they can enroll to a course
// Activity s38

/*router.post("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	//console.log(req.headers.authorization);
	const userData = auth.decode(req.headers.authorization);
	console.log(userData);

	// Provides the user's ID for the getProfile controller method
	//userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));

	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});*/

router.get("/details", auth.verify, (req, res) => {
// Provides the user's ID for the getProfile controller method

    const userData = auth.decode(req.headers.authorization)

    userController.getProfile(userData).then(resultFromController => res.send(resultFromController))
});

// Route for enrolling a user
router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userId : auth.decode(req.headers.authorization).id,
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController));
});


// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;
