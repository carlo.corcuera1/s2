const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoute") // Added from s38 discussion
const courseRoutes = require("./routes/courseRoute") // Added from s39 discussion

const app = express();

const port = 4000;

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://carlo:fNgKw6aWKB1b3jH7@zuitt-bootcamp.nk8pfwk.mongodb.net/s1-s5?retryWrites=true&w=majority", {
		useNewUrlParser: true,
		useUnifiedTopology: true
	});

// Prompts a message in the terminal once the connection is "open" and we are able to successfully connect to our database
mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'));

// Allows all resources to access our backend application

// CORS stands for Cross-Origin Resource Sharing. It is a security mechanism implemented in web browsers to prevent malicious scripts from accessing resources on a different origin (domain, protocol, or port) than the one that served the content.
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Will used the defined port number for the application whenever an environment variable is available OR will used port 4000 if none is defined
// This syntax will allow flexibility when using the application locally or as a hosted application

// app.listen(process.env.PORT || 4000, () => {
//     console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
// });

		

// Defines the "/users" string to be included for all user routes defined in the "user" route file
app.use("/users", userRoutes); // Added from s38 discussion
app.use("/courses", courseRoutes); // Added from s39 discussion



app.listen(port, () => console.log(`API is now online on port ${port}`));