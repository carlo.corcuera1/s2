s37
	- Setup the index.js
	- Create the Course.js model
	- Activity for this session:
		- Create the Users.js model

s38 
	- Create "/user/checkEmail" route & checkEmailExists function in controllers
	- Create "/user/register" route & registerUser function in controllers
		- Install bcrypt
			npm install bcrypt
		- Install jsonwebtoken
			npm install jsonwebtoken
	- Create auth.js
		createAccessToken function
	- Create "/user/login" route & loginUser function in controllers
	- Activity for this session:
		- Create a user route to retrieve the details of the user

s39
	- Create verify function in auth.js
	- Create decode function in auth.js
		- Revise the "/details" routes
	- Create "/course/" route & addCourse function in controllers
	- Activity for this session:
		- Refractor the course route and implement user authentication for admin when creating a course

s40
	- Create "/all" route & getAllCourses function in controllers
	- Create "/active" route & getAllActive  function in controllers
	- Create "/:id" route & getCourse  function in controllers
	- Create "/:id" route & updateCourse  function in controllers
	- Activity for this session:
		- Add an archiving a course function

s41
	- Create "/:id/archive" route & archiveCourse function in controllers
	- Create "/enroll" route & enroll function in controllers
	- Activity for this session:
		- Refractor the user routes to implement user authentication for the enroll route
